import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {



        AlarmData alarmData = new AlarmData();
        List<Detector>detectors = new ArrayList<Detector>();
        List<String>users = new ArrayList<String>();
        for (int i = 0; i < 3 ; i++) {
            String user = "testuser"+i;
            Detector detector = new Detector();
            detector.setTime("00:00");
            detector.setActive("active");
            detector.setDetector_name("detectorname"+i);
            detectors.add(detector);
            users.add(user);
        }
        alarmData.setDetectors(detectors);
        alarmData.setObjectName("testObjectName");
        alarmData.setCity("TESTCity");
        alarmData.setIndicatorPlacement("indicator");
        alarmData.setNumber("0001");
        alarmData.setRoute("pojditudaneznaukuda");
        alarmData.setStatus("active");
        alarmData.setUUID("2143213242453254");
        alarmData.setUseres(users);
        String json = convertUsingGson(alarmData);
        System.out.println(json);

    }
    public static String convertUsingGson(AlarmData alarm)
    {
        Gson gson = new Gson();
        String studentJson = gson.toJson(alarm);
        return studentJson;
    }
}
