import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AlarmData  {
    @SerializedName("objectName")
    @Expose
    private String objectName;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("route")
    @Expose
    private String route;
    @SerializedName("indicatorPlacement")
    @Expose
    private String indicatorPlacement;
    @SerializedName("UUID")
    @Expose
    private String UUID;
    @SerializedName("stautus")
    @Expose
    private String status;
    @SerializedName("users")
    @Expose
    private List<String> useres;
    @SerializedName("detectors")
    @Expose
    private List<Detector> Detectors;


    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setUUID(String UUID) {
        this.UUID = UUID;
    }

    public String getUUID() {
        return UUID;
    }

    public void setUseres(List<String> useres) {
        this.useres = useres;
    }

    public void setDetectors(List<Detector> allDetectors) {
        this.Detectors = allDetectors;
    }

    public String getAddress() {
        return address;
    }

    public List<String> getUseres() {
        return useres;
    }

    public List<Detector> getAllDetectors() {
        return Detectors;
    }


    public String getObjectName() {
        return objectName;
    }


    public String getCity() {
        return city;
    }


    public String getNumber() {
        return number;
    }


    public String getIndicatorPlacement() {
        return indicatorPlacement;
    }


    public String getRoute() {
        return route;
    }


    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }


    public void setAddress(String address) {
        this.address = address;
    }


    public void setCity(String city) {
        this.city = city;
    }


    public void setNumber(String number) {
        this.number = number;
    }


    public void setIndicatorPlacement(String indicatorPlace) {
        this.indicatorPlacement = indicatorPlace;
    }


    public void setRoute(String route) {
        this.route = route;
    }
}