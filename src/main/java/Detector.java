import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Detector {
    @SerializedName("detector_name")
    @Expose
    private String detector_name;
    @SerializedName("active")
    @Expose
    private String active;
    @SerializedName("time")
    @Expose
    private String time;

    public void setDetector_name(String detector_name) {
        this.detector_name = detector_name;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDetector_name() {
        return detector_name;
    }

    public String getActive() {
        return active;
    }

    public String getTime() {
        return time;
    }
}
